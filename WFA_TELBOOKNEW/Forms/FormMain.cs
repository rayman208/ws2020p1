﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFA_TELBOOKNEW
{
    public partial class FormMain : Form
    {
        private void LoadUsersToDataGridViewUsers()
        {
            dataGridViewUsers.DataSource = null;

            List<User> users = TableUsers.GetAllUsers();

            if (users == null)
            {
                MessageBox.Show("Error load users");
                return;
            }

            dataGridViewUsers.DataSource = users;

            dataGridViewUsers.Columns["Id"].HeaderText = "ИД";
            dataGridViewUsers.Columns["Id"].Visible = false;
            dataGridViewUsers.Columns["Name"].HeaderText = "ИМЯ";
            dataGridViewUsers.Columns["Phone"].HeaderText = "ТЕЛЕФОН";
        }

        private void LoadCardsToDataGridViewCards()
        {
            dataGridViewCards.DataSource = null;

            List<Card> cards = TableCards.GetAllCards();

            if (cards == null)
            {
                MessageBox.Show("Error load users");
                return;
            }

            dataGridViewCards.DataSource = cards;

        }

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            LoadUsersToDataGridViewUsers();
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            LoadUsersToDataGridViewUsers();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //textBoxName.Text = textBoxName.Text.Replace(" ", "");

            if (textBoxName.Text == "")
            {
                MessageBox.Show("Error name is empty");
                return;
            }

            if (textBoxPhone.Text == "")
            {
                MessageBox.Show("Error phone is empty");
                return;
            }

            User user = new User()
            {
                Name = textBoxName.Text,
                Phone = textBoxPhone.Text
            };


            bool result = TableUsers.AddUser(user);

            if (result == true)
            {
                MessageBox.Show("Ok add");
                LoadUsersToDataGridViewUsers();

                textBoxName.Clear();
                textBoxPhone.Clear();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void dataGridViewUsers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewUsers.SelectedRows.Count == 0)
            {
                return;
            }

            User user = (User) dataGridViewUsers.SelectedRows[0].DataBoundItem;

            textBoxId.Text = user.Id.ToString();
            textBoxName.Text = user.Name;
            textBoxPhone.Text = user.Phone;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (textBoxId.Text == "")
            {
                MessageBox.Show("Error id is empty");
                return;
            }

            int id = int.Parse(textBoxId.Text);

            bool result = TableUsers.DeleteUser(id);

            if (result == true)
            {
                MessageBox.Show("Ok delete");
                LoadUsersToDataGridViewUsers();

                textBoxId.Clear();
                textBoxName.Clear();
                textBoxPhone.Clear();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void buttonUpdateUser_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "")
            {
                MessageBox.Show("Error name is empty");
                return;
            }

            if (textBoxPhone.Text == "")
            {
                MessageBox.Show("Error phone is empty");
                return;
            }

            if (textBoxId.Text == "")
            {
                MessageBox.Show("Error id is empty");
                return;
            }

            User user = new User()
            {
                Id = int.Parse(textBoxId.Text),
                Name = textBoxName.Text,
                Phone = textBoxPhone.Text
            };


            bool result = TableUsers.UpdateUser(user);

            if (result == true)
            {
                MessageBox.Show("Ok update");
                LoadUsersToDataGridViewUsers();

                textBoxId.Clear();
                textBoxName.Clear();
                textBoxPhone.Clear();
            }
            else
            {
                MessageBox.Show("Fail");
            }
        }

        private void buttonLoadCards_Click(object sender, EventArgs e)
        {
            LoadCardsToDataGridViewCards();
        }

        private void dataGridViewCards_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewCards.SelectedRows.Count == 0)
            {
                return;
            }

            Card card = (Card)dataGridViewCards.SelectedRows[0].DataBoundItem;

            MessageBox.Show(card.Number);

        }
    }
}
