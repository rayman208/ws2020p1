﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WFA_TELBOOKNEW
{
    class TableCards
    {
        public static List<Card> GetAllCards()
        {
            try
            {
                List<Card> cards = new List<Card>();

                MySqlCommand mySqlCommand = DbConnector.GetInstance().GetMySqlCommand();

                mySqlCommand.CommandText = @"SELECT * FROM `cards` AS c JOIN `users` AS u ON c.id_user = u.id";

                MySqlDataReader reader = mySqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    cards.Add(new Card()
                    {
                        Id = reader.GetInt32(0),
                        Number = reader.GetString(1),
                        IdUser = reader.GetInt32(2),
                        Name = reader.GetString(4),
                        Phone = reader.GetString(5)
                    });
                }


                reader.Close();
                return cards;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
