﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WFA_TELBOOKNEW
{
    class TableUsers
    {
        public static List<User> GetAllUsers()
        { 
            try
            {
                List<User> users = new List<User>();

                MySqlCommand mySqlCommand = DbConnector.GetInstance().GetMySqlCommand();

                mySqlCommand.CommandText = "SELECT * FROM `users`";
                MySqlDataReader reader = mySqlCommand.ExecuteReader();

                while (reader.Read() == true)
                {
                    users.Add(new User()
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name"),
                        Phone = reader.GetString("phone")
                    });
                }

                reader.Close();
                return users;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool AddUser(User user)
        {
            try
            {
                MySqlCommand mySqlCommand = DbConnector.GetInstance().GetMySqlCommand();

                mySqlCommand.CommandText = $@"INSERT INTO `users` (`name`,`phone`) 
            VALUES('{user.Name}', '{user.Phone}')";

                mySqlCommand.ExecuteNonQuery();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static bool DeleteUser(int id)
        {
            try
            {
                MySqlCommand mySqlCommand = DbConnector.GetInstance().GetMySqlCommand();

                mySqlCommand.CommandText = $@"DELETE FROM `users` WHERE `id`={id}";

                mySqlCommand.ExecuteNonQuery();

                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public static bool UpdateUser(User user)
        {
            try
            {
                MySqlCommand mySqlCommand = DbConnector.GetInstance().GetMySqlCommand();

                mySqlCommand.CommandText = $@"UPDATE `users` SET `name`='{user.Name}',`phone`='{user.Phone}' WHERE `id`={user.Id}";

                mySqlCommand.ExecuteNonQuery();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
