﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace WFA_TELBOOKNEW
{
    class DbConnector
    {
        private static DbConnector instance = null;

        public static DbConnector GetInstance()
        {
            if (instance == null)
            {
                instance = new DbConnector();
            }
            return instance;
        }

        private MySqlConnection mySqlConnection;
        private MySqlCommand mySqlCommand;

        private DbConnector()
        {
            string connectionString = "server=127.0.0.1;user=root;password=1234;database=telbooknew";
            mySqlConnection = new MySqlConnection(connectionString);
            mySqlConnection.Open();

            mySqlCommand = new MySqlCommand();
            mySqlCommand.Connection = mySqlConnection;
        }

        public MySqlCommand GetMySqlCommand()
        {
            return mySqlCommand;
        }
    }
}
